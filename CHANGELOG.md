 Changelog for "ldap-export-servlet"


## [v1.2.1] [r4.23.0] - 2020-05-29

### Fixes
- This release fixes a potential bug in the export of the users to LDAP. See Incident #19348

## [v1.2.0] [r unknown] - 2016-09-12
### New Features
- Feature #4999: LDAP Export script to export SSH public key

### Fixes
- Bug fix for #4916: LDAP Export fails if user surname is empty

## [v1.2.0] [r unknown] - 2016-07-04
### New Features
- Updated to use Liferay 6.2 API

## [v1.0.0] [r unknown] - 2015-12-11

- First Release
